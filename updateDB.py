from app import db
from app.models import User, Post
import qrcode
from datetime import datetime, time
from random import randrange


def generateQR(number):
    qr = qrcode.QRCode(version=1, box_size=10, border=5)
    qr.add_data(number)
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')
    return img

def addUser(f): # new User
    u = User(place=f.place.data, time_in=f.time_in.data, date=f.date.data,
    time_out=time(randrange(13,17), randrange(10,60)))  # simulate a random time_out time
    db.session.add(u)
    db.session.commit()

def updatePost(f): # new Post
    u = User.query.filter_by(id = 6).first()  # simulate that the post was submitted by User with id=6
    p = Post(title=f.title.data, body=f.body.data, author=u)
    db.session.add(p)
    db.session.commit()

def users_to_notify(f): # users_to_notify
    # get the user given by the NHS
    u = User.query.filter_by(id = f.user_id.data).first()
    t_in = u.time_in
    t_out = u.time_out
    d = u.date
    p = u.place
    # find users that were within the time limits and in the same place
    users = User.query.filter((User.date == d) & (User. place == p) &
    ((User.time_in >= t_in) & (User.time_out <= t_out))).all()
    return users
    