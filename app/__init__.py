from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_datepicker import datepicker
from flask_qrcode import QRcode
from pyngrok import ngrok

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)        # database
migrate = Migrate(app, db)     # migrate engine
bootstrap = Bootstrap(app)
qrcode = QRcode(app)

url = ngrok.connect(5000)
print(url)


from app import routes, models