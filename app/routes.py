from flask import render_template
from app import app, db
from app.forms import PostForm, CheckInForm, NHSForm
from flask import render_template, flash, redirect, url_for, send_file
from datetime import datetime
from app.models import User, Post
from updateDB import addUser, generateQR, updatePost, users_to_notify
from io import BytesIO


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

# Access to make a booking
@app.route('/booking', methods=['GET', 'POST'])
def booking():
    form = CheckInForm()
    if form.validate_on_submit():   # validation check (wtforms)
        flash('Place is {}, Time is {}, Date is {}'.format(
                form.place.data, form.time_in.data, form.date.data))
        flash('QR code stored in your QR codes')   # flash is a message to give feedback to user
        addUser(form)
        return redirect(url_for('booking'))
    return render_template('booking.html', form=form)

# Access to QR code
@app.route('/qrcode')
def qrcode():
    # get the last user object. Simulates the current user logged in
    qr = User.query.order_by(User.id.desc()).first() 
    img_buf = BytesIO()
    img = generateQR(qr)
    img.save(img_buf)  # stores the QR in a binary buffer in memory
    img_buf.seek(0)
    return send_file(img_buf, mimetype='image/png')  # send file to client

# Access to report an issue
@app.route('/reports', methods=['GET', 'POST'])
def report():
    form = PostForm()
    if form.validate_on_submit():
        flash('Your form was submitted. Thank you!')
        updatePost(form)
        return redirect(url_for('report'))
    return render_template('report.html', form=form)

@app.route('/nhs', methods=['GET', 'POST'])
def nhs():
    form = NHSForm()
    if form.validate_on_submit():
        flash('The form was submitted. We will send a notification to sudents at risk.')
        users_to_notify(form)
        return redirect(url_for('nhs'))
    return render_template('nhs.html', form=form)

from app import app, db
from app.models import User, Post

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}


