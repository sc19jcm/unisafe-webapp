from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField, TimeField
from wtforms.widgets import TextArea

# Make a booking form
class CheckInForm(FlaskForm):  # Drop down menus. Validation on required fields
    STATE_CHOICES = [('DEC-10'), ('LONG ROOM'), ('LAID LAW LIBRARY'), ('STUDENT UNION')]
    place = SelectField('Study Area', choices = STATE_CHOICES)
    time_in = TimeField('Time', validators=[DataRequired()])
    date = DateField('Date', format='%Y-%m-%d', validators=[DataRequired()])  
    submit = SubmitField('Check-in!')

# Post form
class PostForm(FlaskForm):  # Validation on required fields
    title = StringField('Title', validators=[DataRequired()])
    body = StringField('Text', widget=TextArea(), validators=[DataRequired()])
    submit = SubmitField('Submit')

class NHSForm(FlaskForm):  # Validation on required fields
    name = StringField('Student Name', validators=[DataRequired()])
    mid_name = StringField('Student Middle Name')
    last_name = StringField('Student Last Name', validators=[DataRequired()])
    user_id = IntegerField('User ID', validators=[DataRequired()])
    submit = SubmitField('Submit')