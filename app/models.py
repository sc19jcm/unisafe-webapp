from datetime import datetime
from app import db

# User table
class User(db.Model):   #db.Model, a base class for all models from Flask-SQLAlchemy
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DATE, index=True)   # index fields to make efficient queries
    time_in = db.Column(db.Time, index=True)
    time_out = db.Column(db.Time, index=True)
    place = db.Column(db.String(120), index=True)
    posts = db.relationship('Post', backref='author', lazy='dynamic') # connects user with the post

    def __repr__(self):
        return '<User {}. Date {}. Time_in {}. Time_out {}. Place {}.>\n'.format(self.id, self.date, 
        self.time_in, self.time_out, self.place)

# Post table
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    body = db.Column(db.String(300))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id')) # foreign key to id from User table

    def __repr__(self):
        return '<Title {}. Body {}. Timestamp {}. User_id {}>\n'.format(self.title, self.body,
        self.timestamp, self.user_id)